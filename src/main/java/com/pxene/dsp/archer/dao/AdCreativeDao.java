package com.pxene.dsp.archer.dao;

import com.pxene.dsp.archer.model.AdCreativeModel;
import com.pxene.dsp.archer.model.AdCreativeModelExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AdCreativeDao {
    int countByExample(AdCreativeModelExample example);

    int deleteByExample(AdCreativeModelExample example);

    int deleteByPrimaryKey(String id);

    int insert(AdCreativeModel record);

    int insertSelective(AdCreativeModel record);

    List<AdCreativeModel> selectByExample(AdCreativeModelExample example);

    AdCreativeModel selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") AdCreativeModel record, @Param("example") AdCreativeModelExample example);

    int updateByExample(@Param("record") AdCreativeModel record, @Param("example") AdCreativeModelExample example);

    int updateByPrimaryKeySelective(AdCreativeModel record);

    int updateByPrimaryKey(AdCreativeModel record);
}