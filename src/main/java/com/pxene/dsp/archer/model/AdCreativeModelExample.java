package com.pxene.dsp.archer.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AdCreativeModelExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AdCreativeModelExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andAdxidIsNull() {
            addCriterion("adxid is null");
            return (Criteria) this;
        }

        public Criteria andAdxidIsNotNull() {
            addCriterion("adxid is not null");
            return (Criteria) this;
        }

        public Criteria andAdxidEqualTo(String value) {
            addCriterion("adxid =", value, "adxid");
            return (Criteria) this;
        }

        public Criteria andAdxidNotEqualTo(String value) {
            addCriterion("adxid <>", value, "adxid");
            return (Criteria) this;
        }

        public Criteria andAdxidGreaterThan(String value) {
            addCriterion("adxid >", value, "adxid");
            return (Criteria) this;
        }

        public Criteria andAdxidGreaterThanOrEqualTo(String value) {
            addCriterion("adxid >=", value, "adxid");
            return (Criteria) this;
        }

        public Criteria andAdxidLessThan(String value) {
            addCriterion("adxid <", value, "adxid");
            return (Criteria) this;
        }

        public Criteria andAdxidLessThanOrEqualTo(String value) {
            addCriterion("adxid <=", value, "adxid");
            return (Criteria) this;
        }

        public Criteria andAdxidLike(String value) {
            addCriterion("adxid like", value, "adxid");
            return (Criteria) this;
        }

        public Criteria andAdxidNotLike(String value) {
            addCriterion("adxid not like", value, "adxid");
            return (Criteria) this;
        }

        public Criteria andAdxidIn(List<String> values) {
            addCriterion("adxid in", values, "adxid");
            return (Criteria) this;
        }

        public Criteria andAdxidNotIn(List<String> values) {
            addCriterion("adxid not in", values, "adxid");
            return (Criteria) this;
        }

        public Criteria andAdxidBetween(String value1, String value2) {
            addCriterion("adxid between", value1, value2, "adxid");
            return (Criteria) this;
        }

        public Criteria andAdxidNotBetween(String value1, String value2) {
            addCriterion("adxid not between", value1, value2, "adxid");
            return (Criteria) this;
        }

        public Criteria andAdtypeIsNull() {
            addCriterion("adtype is null");
            return (Criteria) this;
        }

        public Criteria andAdtypeIsNotNull() {
            addCriterion("adtype is not null");
            return (Criteria) this;
        }

        public Criteria andAdtypeEqualTo(String value) {
            addCriterion("adtype =", value, "adtype");
            return (Criteria) this;
        }

        public Criteria andAdtypeNotEqualTo(String value) {
            addCriterion("adtype <>", value, "adtype");
            return (Criteria) this;
        }

        public Criteria andAdtypeGreaterThan(String value) {
            addCriterion("adtype >", value, "adtype");
            return (Criteria) this;
        }

        public Criteria andAdtypeGreaterThanOrEqualTo(String value) {
            addCriterion("adtype >=", value, "adtype");
            return (Criteria) this;
        }

        public Criteria andAdtypeLessThan(String value) {
            addCriterion("adtype <", value, "adtype");
            return (Criteria) this;
        }

        public Criteria andAdtypeLessThanOrEqualTo(String value) {
            addCriterion("adtype <=", value, "adtype");
            return (Criteria) this;
        }

        public Criteria andAdtypeLike(String value) {
            addCriterion("adtype like", value, "adtype");
            return (Criteria) this;
        }

        public Criteria andAdtypeNotLike(String value) {
            addCriterion("adtype not like", value, "adtype");
            return (Criteria) this;
        }

        public Criteria andAdtypeIn(List<String> values) {
            addCriterion("adtype in", values, "adtype");
            return (Criteria) this;
        }

        public Criteria andAdtypeNotIn(List<String> values) {
            addCriterion("adtype not in", values, "adtype");
            return (Criteria) this;
        }

        public Criteria andAdtypeBetween(String value1, String value2) {
            addCriterion("adtype between", value1, value2, "adtype");
            return (Criteria) this;
        }

        public Criteria andAdtypeNotBetween(String value1, String value2) {
            addCriterion("adtype not between", value1, value2, "adtype");
            return (Criteria) this;
        }

        public Criteria andTxttitleIsNull() {
            addCriterion("txttitle is null");
            return (Criteria) this;
        }

        public Criteria andTxttitleIsNotNull() {
            addCriterion("txttitle is not null");
            return (Criteria) this;
        }

        public Criteria andTxttitleEqualTo(String value) {
            addCriterion("txttitle =", value, "txttitle");
            return (Criteria) this;
        }

        public Criteria andTxttitleNotEqualTo(String value) {
            addCriterion("txttitle <>", value, "txttitle");
            return (Criteria) this;
        }

        public Criteria andTxttitleGreaterThan(String value) {
            addCriterion("txttitle >", value, "txttitle");
            return (Criteria) this;
        }

        public Criteria andTxttitleGreaterThanOrEqualTo(String value) {
            addCriterion("txttitle >=", value, "txttitle");
            return (Criteria) this;
        }

        public Criteria andTxttitleLessThan(String value) {
            addCriterion("txttitle <", value, "txttitle");
            return (Criteria) this;
        }

        public Criteria andTxttitleLessThanOrEqualTo(String value) {
            addCriterion("txttitle <=", value, "txttitle");
            return (Criteria) this;
        }

        public Criteria andTxttitleLike(String value) {
            addCriterion("txttitle like", value, "txttitle");
            return (Criteria) this;
        }

        public Criteria andTxttitleNotLike(String value) {
            addCriterion("txttitle not like", value, "txttitle");
            return (Criteria) this;
        }

        public Criteria andTxttitleIn(List<String> values) {
            addCriterion("txttitle in", values, "txttitle");
            return (Criteria) this;
        }

        public Criteria andTxttitleNotIn(List<String> values) {
            addCriterion("txttitle not in", values, "txttitle");
            return (Criteria) this;
        }

        public Criteria andTxttitleBetween(String value1, String value2) {
            addCriterion("txttitle between", value1, value2, "txttitle");
            return (Criteria) this;
        }

        public Criteria andTxttitleNotBetween(String value1, String value2) {
            addCriterion("txttitle not between", value1, value2, "txttitle");
            return (Criteria) this;
        }

        public Criteria andTxtfirstIsNull() {
            addCriterion("txtfirst is null");
            return (Criteria) this;
        }

        public Criteria andTxtfirstIsNotNull() {
            addCriterion("txtfirst is not null");
            return (Criteria) this;
        }

        public Criteria andTxtfirstEqualTo(String value) {
            addCriterion("txtfirst =", value, "txtfirst");
            return (Criteria) this;
        }

        public Criteria andTxtfirstNotEqualTo(String value) {
            addCriterion("txtfirst <>", value, "txtfirst");
            return (Criteria) this;
        }

        public Criteria andTxtfirstGreaterThan(String value) {
            addCriterion("txtfirst >", value, "txtfirst");
            return (Criteria) this;
        }

        public Criteria andTxtfirstGreaterThanOrEqualTo(String value) {
            addCriterion("txtfirst >=", value, "txtfirst");
            return (Criteria) this;
        }

        public Criteria andTxtfirstLessThan(String value) {
            addCriterion("txtfirst <", value, "txtfirst");
            return (Criteria) this;
        }

        public Criteria andTxtfirstLessThanOrEqualTo(String value) {
            addCriterion("txtfirst <=", value, "txtfirst");
            return (Criteria) this;
        }

        public Criteria andTxtfirstLike(String value) {
            addCriterion("txtfirst like", value, "txtfirst");
            return (Criteria) this;
        }

        public Criteria andTxtfirstNotLike(String value) {
            addCriterion("txtfirst not like", value, "txtfirst");
            return (Criteria) this;
        }

        public Criteria andTxtfirstIn(List<String> values) {
            addCriterion("txtfirst in", values, "txtfirst");
            return (Criteria) this;
        }

        public Criteria andTxtfirstNotIn(List<String> values) {
            addCriterion("txtfirst not in", values, "txtfirst");
            return (Criteria) this;
        }

        public Criteria andTxtfirstBetween(String value1, String value2) {
            addCriterion("txtfirst between", value1, value2, "txtfirst");
            return (Criteria) this;
        }

        public Criteria andTxtfirstNotBetween(String value1, String value2) {
            addCriterion("txtfirst not between", value1, value2, "txtfirst");
            return (Criteria) this;
        }

        public Criteria andCbundleIsNull() {
            addCriterion("cbundle is null");
            return (Criteria) this;
        }

        public Criteria andCbundleIsNotNull() {
            addCriterion("cbundle is not null");
            return (Criteria) this;
        }

        public Criteria andCbundleEqualTo(String value) {
            addCriterion("cbundle =", value, "cbundle");
            return (Criteria) this;
        }

        public Criteria andCbundleNotEqualTo(String value) {
            addCriterion("cbundle <>", value, "cbundle");
            return (Criteria) this;
        }

        public Criteria andCbundleGreaterThan(String value) {
            addCriterion("cbundle >", value, "cbundle");
            return (Criteria) this;
        }

        public Criteria andCbundleGreaterThanOrEqualTo(String value) {
            addCriterion("cbundle >=", value, "cbundle");
            return (Criteria) this;
        }

        public Criteria andCbundleLessThan(String value) {
            addCriterion("cbundle <", value, "cbundle");
            return (Criteria) this;
        }

        public Criteria andCbundleLessThanOrEqualTo(String value) {
            addCriterion("cbundle <=", value, "cbundle");
            return (Criteria) this;
        }

        public Criteria andCbundleLike(String value) {
            addCriterion("cbundle like", value, "cbundle");
            return (Criteria) this;
        }

        public Criteria andCbundleNotLike(String value) {
            addCriterion("cbundle not like", value, "cbundle");
            return (Criteria) this;
        }

        public Criteria andCbundleIn(List<String> values) {
            addCriterion("cbundle in", values, "cbundle");
            return (Criteria) this;
        }

        public Criteria andCbundleNotIn(List<String> values) {
            addCriterion("cbundle not in", values, "cbundle");
            return (Criteria) this;
        }

        public Criteria andCbundleBetween(String value1, String value2) {
            addCriterion("cbundle between", value1, value2, "cbundle");
            return (Criteria) this;
        }

        public Criteria andCbundleNotBetween(String value1, String value2) {
            addCriterion("cbundle not between", value1, value2, "cbundle");
            return (Criteria) this;
        }

        public Criteria andApknameIsNull() {
            addCriterion("apkname is null");
            return (Criteria) this;
        }

        public Criteria andApknameIsNotNull() {
            addCriterion("apkname is not null");
            return (Criteria) this;
        }

        public Criteria andApknameEqualTo(String value) {
            addCriterion("apkname =", value, "apkname");
            return (Criteria) this;
        }

        public Criteria andApknameNotEqualTo(String value) {
            addCriterion("apkname <>", value, "apkname");
            return (Criteria) this;
        }

        public Criteria andApknameGreaterThan(String value) {
            addCriterion("apkname >", value, "apkname");
            return (Criteria) this;
        }

        public Criteria andApknameGreaterThanOrEqualTo(String value) {
            addCriterion("apkname >=", value, "apkname");
            return (Criteria) this;
        }

        public Criteria andApknameLessThan(String value) {
            addCriterion("apkname <", value, "apkname");
            return (Criteria) this;
        }

        public Criteria andApknameLessThanOrEqualTo(String value) {
            addCriterion("apkname <=", value, "apkname");
            return (Criteria) this;
        }

        public Criteria andApknameLike(String value) {
            addCriterion("apkname like", value, "apkname");
            return (Criteria) this;
        }

        public Criteria andApknameNotLike(String value) {
            addCriterion("apkname not like", value, "apkname");
            return (Criteria) this;
        }

        public Criteria andApknameIn(List<String> values) {
            addCriterion("apkname in", values, "apkname");
            return (Criteria) this;
        }

        public Criteria andApknameNotIn(List<String> values) {
            addCriterion("apkname not in", values, "apkname");
            return (Criteria) this;
        }

        public Criteria andApknameBetween(String value1, String value2) {
            addCriterion("apkname between", value1, value2, "apkname");
            return (Criteria) this;
        }

        public Criteria andApknameNotBetween(String value1, String value2) {
            addCriterion("apkname not between", value1, value2, "apkname");
            return (Criteria) this;
        }

        public Criteria andTxtsecondIsNull() {
            addCriterion("txtsecond is null");
            return (Criteria) this;
        }

        public Criteria andTxtsecondIsNotNull() {
            addCriterion("txtsecond is not null");
            return (Criteria) this;
        }

        public Criteria andTxtsecondEqualTo(String value) {
            addCriterion("txtsecond =", value, "txtsecond");
            return (Criteria) this;
        }

        public Criteria andTxtsecondNotEqualTo(String value) {
            addCriterion("txtsecond <>", value, "txtsecond");
            return (Criteria) this;
        }

        public Criteria andTxtsecondGreaterThan(String value) {
            addCriterion("txtsecond >", value, "txtsecond");
            return (Criteria) this;
        }

        public Criteria andTxtsecondGreaterThanOrEqualTo(String value) {
            addCriterion("txtsecond >=", value, "txtsecond");
            return (Criteria) this;
        }

        public Criteria andTxtsecondLessThan(String value) {
            addCriterion("txtsecond <", value, "txtsecond");
            return (Criteria) this;
        }

        public Criteria andTxtsecondLessThanOrEqualTo(String value) {
            addCriterion("txtsecond <=", value, "txtsecond");
            return (Criteria) this;
        }

        public Criteria andTxtsecondLike(String value) {
            addCriterion("txtsecond like", value, "txtsecond");
            return (Criteria) this;
        }

        public Criteria andTxtsecondNotLike(String value) {
            addCriterion("txtsecond not like", value, "txtsecond");
            return (Criteria) this;
        }

        public Criteria andTxtsecondIn(List<String> values) {
            addCriterion("txtsecond in", values, "txtsecond");
            return (Criteria) this;
        }

        public Criteria andTxtsecondNotIn(List<String> values) {
            addCriterion("txtsecond not in", values, "txtsecond");
            return (Criteria) this;
        }

        public Criteria andTxtsecondBetween(String value1, String value2) {
            addCriterion("txtsecond between", value1, value2, "txtsecond");
            return (Criteria) this;
        }

        public Criteria andTxtsecondNotBetween(String value1, String value2) {
            addCriterion("txtsecond not between", value1, value2, "txtsecond");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkIsNull() {
            addCriterion("txtshowlink is null");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkIsNotNull() {
            addCriterion("txtshowlink is not null");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkEqualTo(String value) {
            addCriterion("txtshowlink =", value, "txtshowlink");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkNotEqualTo(String value) {
            addCriterion("txtshowlink <>", value, "txtshowlink");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkGreaterThan(String value) {
            addCriterion("txtshowlink >", value, "txtshowlink");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkGreaterThanOrEqualTo(String value) {
            addCriterion("txtshowlink >=", value, "txtshowlink");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkLessThan(String value) {
            addCriterion("txtshowlink <", value, "txtshowlink");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkLessThanOrEqualTo(String value) {
            addCriterion("txtshowlink <=", value, "txtshowlink");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkLike(String value) {
            addCriterion("txtshowlink like", value, "txtshowlink");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkNotLike(String value) {
            addCriterion("txtshowlink not like", value, "txtshowlink");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkIn(List<String> values) {
            addCriterion("txtshowlink in", values, "txtshowlink");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkNotIn(List<String> values) {
            addCriterion("txtshowlink not in", values, "txtshowlink");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkBetween(String value1, String value2) {
            addCriterion("txtshowlink between", value1, value2, "txtshowlink");
            return (Criteria) this;
        }

        public Criteria andTxtshowlinkNotBetween(String value1, String value2) {
            addCriterion("txtshowlink not between", value1, value2, "txtshowlink");
            return (Criteria) this;
        }

        public Criteria andTimelengthIsNull() {
            addCriterion("timelength is null");
            return (Criteria) this;
        }

        public Criteria andTimelengthIsNotNull() {
            addCriterion("timelength is not null");
            return (Criteria) this;
        }

        public Criteria andTimelengthEqualTo(Integer value) {
            addCriterion("timelength =", value, "timelength");
            return (Criteria) this;
        }

        public Criteria andTimelengthNotEqualTo(Integer value) {
            addCriterion("timelength <>", value, "timelength");
            return (Criteria) this;
        }

        public Criteria andTimelengthGreaterThan(Integer value) {
            addCriterion("timelength >", value, "timelength");
            return (Criteria) this;
        }

        public Criteria andTimelengthGreaterThanOrEqualTo(Integer value) {
            addCriterion("timelength >=", value, "timelength");
            return (Criteria) this;
        }

        public Criteria andTimelengthLessThan(Integer value) {
            addCriterion("timelength <", value, "timelength");
            return (Criteria) this;
        }

        public Criteria andTimelengthLessThanOrEqualTo(Integer value) {
            addCriterion("timelength <=", value, "timelength");
            return (Criteria) this;
        }

        public Criteria andTimelengthIn(List<Integer> values) {
            addCriterion("timelength in", values, "timelength");
            return (Criteria) this;
        }

        public Criteria andTimelengthNotIn(List<Integer> values) {
            addCriterion("timelength not in", values, "timelength");
            return (Criteria) this;
        }

        public Criteria andTimelengthBetween(Integer value1, Integer value2) {
            addCriterion("timelength between", value1, value2, "timelength");
            return (Criteria) this;
        }

        public Criteria andTimelengthNotBetween(Integer value1, Integer value2) {
            addCriterion("timelength not between", value1, value2, "timelength");
            return (Criteria) this;
        }

        public Criteria andShowlinkIsNull() {
            addCriterion("showlink is null");
            return (Criteria) this;
        }

        public Criteria andShowlinkIsNotNull() {
            addCriterion("showlink is not null");
            return (Criteria) this;
        }

        public Criteria andShowlinkEqualTo(String value) {
            addCriterion("showlink =", value, "showlink");
            return (Criteria) this;
        }

        public Criteria andShowlinkNotEqualTo(String value) {
            addCriterion("showlink <>", value, "showlink");
            return (Criteria) this;
        }

        public Criteria andShowlinkGreaterThan(String value) {
            addCriterion("showlink >", value, "showlink");
            return (Criteria) this;
        }

        public Criteria andShowlinkGreaterThanOrEqualTo(String value) {
            addCriterion("showlink >=", value, "showlink");
            return (Criteria) this;
        }

        public Criteria andShowlinkLessThan(String value) {
            addCriterion("showlink <", value, "showlink");
            return (Criteria) this;
        }

        public Criteria andShowlinkLessThanOrEqualTo(String value) {
            addCriterion("showlink <=", value, "showlink");
            return (Criteria) this;
        }

        public Criteria andShowlinkLike(String value) {
            addCriterion("showlink like", value, "showlink");
            return (Criteria) this;
        }

        public Criteria andShowlinkNotLike(String value) {
            addCriterion("showlink not like", value, "showlink");
            return (Criteria) this;
        }

        public Criteria andShowlinkIn(List<String> values) {
            addCriterion("showlink in", values, "showlink");
            return (Criteria) this;
        }

        public Criteria andShowlinkNotIn(List<String> values) {
            addCriterion("showlink not in", values, "showlink");
            return (Criteria) this;
        }

        public Criteria andShowlinkBetween(String value1, String value2) {
            addCriterion("showlink between", value1, value2, "showlink");
            return (Criteria) this;
        }

        public Criteria andShowlinkNotBetween(String value1, String value2) {
            addCriterion("showlink not between", value1, value2, "showlink");
            return (Criteria) this;
        }

        public Criteria andClicklinkIsNull() {
            addCriterion("clicklink is null");
            return (Criteria) this;
        }

        public Criteria andClicklinkIsNotNull() {
            addCriterion("clicklink is not null");
            return (Criteria) this;
        }

        public Criteria andClicklinkEqualTo(String value) {
            addCriterion("clicklink =", value, "clicklink");
            return (Criteria) this;
        }

        public Criteria andClicklinkNotEqualTo(String value) {
            addCriterion("clicklink <>", value, "clicklink");
            return (Criteria) this;
        }

        public Criteria andClicklinkGreaterThan(String value) {
            addCriterion("clicklink >", value, "clicklink");
            return (Criteria) this;
        }

        public Criteria andClicklinkGreaterThanOrEqualTo(String value) {
            addCriterion("clicklink >=", value, "clicklink");
            return (Criteria) this;
        }

        public Criteria andClicklinkLessThan(String value) {
            addCriterion("clicklink <", value, "clicklink");
            return (Criteria) this;
        }

        public Criteria andClicklinkLessThanOrEqualTo(String value) {
            addCriterion("clicklink <=", value, "clicklink");
            return (Criteria) this;
        }

        public Criteria andClicklinkLike(String value) {
            addCriterion("clicklink like", value, "clicklink");
            return (Criteria) this;
        }

        public Criteria andClicklinkNotLike(String value) {
            addCriterion("clicklink not like", value, "clicklink");
            return (Criteria) this;
        }

        public Criteria andClicklinkIn(List<String> values) {
            addCriterion("clicklink in", values, "clicklink");
            return (Criteria) this;
        }

        public Criteria andClicklinkNotIn(List<String> values) {
            addCriterion("clicklink not in", values, "clicklink");
            return (Criteria) this;
        }

        public Criteria andClicklinkBetween(String value1, String value2) {
            addCriterion("clicklink between", value1, value2, "clicklink");
            return (Criteria) this;
        }

        public Criteria andClicklinkNotBetween(String value1, String value2) {
            addCriterion("clicklink not between", value1, value2, "clicklink");
            return (Criteria) this;
        }

        public Criteria andMaterialidIsNull() {
            addCriterion("materialid is null");
            return (Criteria) this;
        }

        public Criteria andMaterialidIsNotNull() {
            addCriterion("materialid is not null");
            return (Criteria) this;
        }

        public Criteria andMaterialidEqualTo(String value) {
            addCriterion("materialid =", value, "materialid");
            return (Criteria) this;
        }

        public Criteria andMaterialidNotEqualTo(String value) {
            addCriterion("materialid <>", value, "materialid");
            return (Criteria) this;
        }

        public Criteria andMaterialidGreaterThan(String value) {
            addCriterion("materialid >", value, "materialid");
            return (Criteria) this;
        }

        public Criteria andMaterialidGreaterThanOrEqualTo(String value) {
            addCriterion("materialid >=", value, "materialid");
            return (Criteria) this;
        }

        public Criteria andMaterialidLessThan(String value) {
            addCriterion("materialid <", value, "materialid");
            return (Criteria) this;
        }

        public Criteria andMaterialidLessThanOrEqualTo(String value) {
            addCriterion("materialid <=", value, "materialid");
            return (Criteria) this;
        }

        public Criteria andMaterialidLike(String value) {
            addCriterion("materialid like", value, "materialid");
            return (Criteria) this;
        }

        public Criteria andMaterialidNotLike(String value) {
            addCriterion("materialid not like", value, "materialid");
            return (Criteria) this;
        }

        public Criteria andMaterialidIn(List<String> values) {
            addCriterion("materialid in", values, "materialid");
            return (Criteria) this;
        }

        public Criteria andMaterialidNotIn(List<String> values) {
            addCriterion("materialid not in", values, "materialid");
            return (Criteria) this;
        }

        public Criteria andMaterialidBetween(String value1, String value2) {
            addCriterion("materialid between", value1, value2, "materialid");
            return (Criteria) this;
        }

        public Criteria andMaterialidNotBetween(String value1, String value2) {
            addCriterion("materialid not between", value1, value2, "materialid");
            return (Criteria) this;
        }

        public Criteria andCategorytypeIsNull() {
            addCriterion("categorytype is null");
            return (Criteria) this;
        }

        public Criteria andCategorytypeIsNotNull() {
            addCriterion("categorytype is not null");
            return (Criteria) this;
        }

        public Criteria andCategorytypeEqualTo(String value) {
            addCriterion("categorytype =", value, "categorytype");
            return (Criteria) this;
        }

        public Criteria andCategorytypeNotEqualTo(String value) {
            addCriterion("categorytype <>", value, "categorytype");
            return (Criteria) this;
        }

        public Criteria andCategorytypeGreaterThan(String value) {
            addCriterion("categorytype >", value, "categorytype");
            return (Criteria) this;
        }

        public Criteria andCategorytypeGreaterThanOrEqualTo(String value) {
            addCriterion("categorytype >=", value, "categorytype");
            return (Criteria) this;
        }

        public Criteria andCategorytypeLessThan(String value) {
            addCriterion("categorytype <", value, "categorytype");
            return (Criteria) this;
        }

        public Criteria andCategorytypeLessThanOrEqualTo(String value) {
            addCriterion("categorytype <=", value, "categorytype");
            return (Criteria) this;
        }

        public Criteria andCategorytypeLike(String value) {
            addCriterion("categorytype like", value, "categorytype");
            return (Criteria) this;
        }

        public Criteria andCategorytypeNotLike(String value) {
            addCriterion("categorytype not like", value, "categorytype");
            return (Criteria) this;
        }

        public Criteria andCategorytypeIn(List<String> values) {
            addCriterion("categorytype in", values, "categorytype");
            return (Criteria) this;
        }

        public Criteria andCategorytypeNotIn(List<String> values) {
            addCriterion("categorytype not in", values, "categorytype");
            return (Criteria) this;
        }

        public Criteria andCategorytypeBetween(String value1, String value2) {
            addCriterion("categorytype between", value1, value2, "categorytype");
            return (Criteria) this;
        }

        public Criteria andCategorytypeNotBetween(String value1, String value2) {
            addCriterion("categorytype not between", value1, value2, "categorytype");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("flag is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("flag is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("flag =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("flag <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("flag >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("flag >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("flag <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("flag <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("flag like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("flag not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("flag in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("flag not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("flag between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("flag not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createtime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createtime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Date value) {
            addCriterion("createtime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Date value) {
            addCriterion("createtime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Date value) {
            addCriterion("createtime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createtime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Date value) {
            addCriterion("createtime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Date value) {
            addCriterion("createtime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Date> values) {
            addCriterion("createtime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Date> values) {
            addCriterion("createtime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Date value1, Date value2) {
            addCriterion("createtime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Date value1, Date value2) {
            addCriterion("createtime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNull() {
            addCriterion("updatetime is null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNotNull() {
            addCriterion("updatetime is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeEqualTo(Date value) {
            addCriterion("updatetime =", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotEqualTo(Date value) {
            addCriterion("updatetime <>", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThan(Date value) {
            addCriterion("updatetime >", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("updatetime >=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThan(Date value) {
            addCriterion("updatetime <", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThanOrEqualTo(Date value) {
            addCriterion("updatetime <=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIn(List<Date> values) {
            addCriterion("updatetime in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotIn(List<Date> values) {
            addCriterion("updatetime not in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeBetween(Date value1, Date value2) {
            addCriterion("updatetime between", value1, value2, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotBetween(Date value1, Date value2) {
            addCriterion("updatetime not between", value1, value2, "updatetime");
            return (Criteria) this;
        }

        public Criteria andAppdescIsNull() {
            addCriterion("appdesc is null");
            return (Criteria) this;
        }

        public Criteria andAppdescIsNotNull() {
            addCriterion("appdesc is not null");
            return (Criteria) this;
        }

        public Criteria andAppdescEqualTo(String value) {
            addCriterion("appdesc =", value, "appdesc");
            return (Criteria) this;
        }

        public Criteria andAppdescNotEqualTo(String value) {
            addCriterion("appdesc <>", value, "appdesc");
            return (Criteria) this;
        }

        public Criteria andAppdescGreaterThan(String value) {
            addCriterion("appdesc >", value, "appdesc");
            return (Criteria) this;
        }

        public Criteria andAppdescGreaterThanOrEqualTo(String value) {
            addCriterion("appdesc >=", value, "appdesc");
            return (Criteria) this;
        }

        public Criteria andAppdescLessThan(String value) {
            addCriterion("appdesc <", value, "appdesc");
            return (Criteria) this;
        }

        public Criteria andAppdescLessThanOrEqualTo(String value) {
            addCriterion("appdesc <=", value, "appdesc");
            return (Criteria) this;
        }

        public Criteria andAppdescLike(String value) {
            addCriterion("appdesc like", value, "appdesc");
            return (Criteria) this;
        }

        public Criteria andAppdescNotLike(String value) {
            addCriterion("appdesc not like", value, "appdesc");
            return (Criteria) this;
        }

        public Criteria andAppdescIn(List<String> values) {
            addCriterion("appdesc in", values, "appdesc");
            return (Criteria) this;
        }

        public Criteria andAppdescNotIn(List<String> values) {
            addCriterion("appdesc not in", values, "appdesc");
            return (Criteria) this;
        }

        public Criteria andAppdescBetween(String value1, String value2) {
            addCriterion("appdesc between", value1, value2, "appdesc");
            return (Criteria) this;
        }

        public Criteria andAppdescNotBetween(String value1, String value2) {
            addCriterion("appdesc not between", value1, value2, "appdesc");
            return (Criteria) this;
        }

        public Criteria andApppackagesizeIsNull() {
            addCriterion("apppackagesize is null");
            return (Criteria) this;
        }

        public Criteria andApppackagesizeIsNotNull() {
            addCriterion("apppackagesize is not null");
            return (Criteria) this;
        }

        public Criteria andApppackagesizeEqualTo(Float value) {
            addCriterion("apppackagesize =", value, "apppackagesize");
            return (Criteria) this;
        }

        public Criteria andApppackagesizeNotEqualTo(Float value) {
            addCriterion("apppackagesize <>", value, "apppackagesize");
            return (Criteria) this;
        }

        public Criteria andApppackagesizeGreaterThan(Float value) {
            addCriterion("apppackagesize >", value, "apppackagesize");
            return (Criteria) this;
        }

        public Criteria andApppackagesizeGreaterThanOrEqualTo(Float value) {
            addCriterion("apppackagesize >=", value, "apppackagesize");
            return (Criteria) this;
        }

        public Criteria andApppackagesizeLessThan(Float value) {
            addCriterion("apppackagesize <", value, "apppackagesize");
            return (Criteria) this;
        }

        public Criteria andApppackagesizeLessThanOrEqualTo(Float value) {
            addCriterion("apppackagesize <=", value, "apppackagesize");
            return (Criteria) this;
        }

        public Criteria andApppackagesizeIn(List<Float> values) {
            addCriterion("apppackagesize in", values, "apppackagesize");
            return (Criteria) this;
        }

        public Criteria andApppackagesizeNotIn(List<Float> values) {
            addCriterion("apppackagesize not in", values, "apppackagesize");
            return (Criteria) this;
        }

        public Criteria andApppackagesizeBetween(Float value1, Float value2) {
            addCriterion("apppackagesize between", value1, value2, "apppackagesize");
            return (Criteria) this;
        }

        public Criteria andApppackagesizeNotBetween(Float value1, Float value2) {
            addCriterion("apppackagesize not between", value1, value2, "apppackagesize");
            return (Criteria) this;
        }

        public Criteria andIconsourceidIsNull() {
            addCriterion("iconsourceid is null");
            return (Criteria) this;
        }

        public Criteria andIconsourceidIsNotNull() {
            addCriterion("iconsourceid is not null");
            return (Criteria) this;
        }

        public Criteria andIconsourceidEqualTo(String value) {
            addCriterion("iconsourceid =", value, "iconsourceid");
            return (Criteria) this;
        }

        public Criteria andIconsourceidNotEqualTo(String value) {
            addCriterion("iconsourceid <>", value, "iconsourceid");
            return (Criteria) this;
        }

        public Criteria andIconsourceidGreaterThan(String value) {
            addCriterion("iconsourceid >", value, "iconsourceid");
            return (Criteria) this;
        }

        public Criteria andIconsourceidGreaterThanOrEqualTo(String value) {
            addCriterion("iconsourceid >=", value, "iconsourceid");
            return (Criteria) this;
        }

        public Criteria andIconsourceidLessThan(String value) {
            addCriterion("iconsourceid <", value, "iconsourceid");
            return (Criteria) this;
        }

        public Criteria andIconsourceidLessThanOrEqualTo(String value) {
            addCriterion("iconsourceid <=", value, "iconsourceid");
            return (Criteria) this;
        }

        public Criteria andIconsourceidLike(String value) {
            addCriterion("iconsourceid like", value, "iconsourceid");
            return (Criteria) this;
        }

        public Criteria andIconsourceidNotLike(String value) {
            addCriterion("iconsourceid not like", value, "iconsourceid");
            return (Criteria) this;
        }

        public Criteria andIconsourceidIn(List<String> values) {
            addCriterion("iconsourceid in", values, "iconsourceid");
            return (Criteria) this;
        }

        public Criteria andIconsourceidNotIn(List<String> values) {
            addCriterion("iconsourceid not in", values, "iconsourceid");
            return (Criteria) this;
        }

        public Criteria andIconsourceidBetween(String value1, String value2) {
            addCriterion("iconsourceid between", value1, value2, "iconsourceid");
            return (Criteria) this;
        }

        public Criteria andIconsourceidNotBetween(String value1, String value2) {
            addCriterion("iconsourceid not between", value1, value2, "iconsourceid");
            return (Criteria) this;
        }

        public Criteria andVideosizeIsNull() {
            addCriterion("videosize is null");
            return (Criteria) this;
        }

        public Criteria andVideosizeIsNotNull() {
            addCriterion("videosize is not null");
            return (Criteria) this;
        }

        public Criteria andVideosizeEqualTo(String value) {
            addCriterion("videosize =", value, "videosize");
            return (Criteria) this;
        }

        public Criteria andVideosizeNotEqualTo(String value) {
            addCriterion("videosize <>", value, "videosize");
            return (Criteria) this;
        }

        public Criteria andVideosizeGreaterThan(String value) {
            addCriterion("videosize >", value, "videosize");
            return (Criteria) this;
        }

        public Criteria andVideosizeGreaterThanOrEqualTo(String value) {
            addCriterion("videosize >=", value, "videosize");
            return (Criteria) this;
        }

        public Criteria andVideosizeLessThan(String value) {
            addCriterion("videosize <", value, "videosize");
            return (Criteria) this;
        }

        public Criteria andVideosizeLessThanOrEqualTo(String value) {
            addCriterion("videosize <=", value, "videosize");
            return (Criteria) this;
        }

        public Criteria andVideosizeLike(String value) {
            addCriterion("videosize like", value, "videosize");
            return (Criteria) this;
        }

        public Criteria andVideosizeNotLike(String value) {
            addCriterion("videosize not like", value, "videosize");
            return (Criteria) this;
        }

        public Criteria andVideosizeIn(List<String> values) {
            addCriterion("videosize in", values, "videosize");
            return (Criteria) this;
        }

        public Criteria andVideosizeNotIn(List<String> values) {
            addCriterion("videosize not in", values, "videosize");
            return (Criteria) this;
        }

        public Criteria andVideosizeBetween(String value1, String value2) {
            addCriterion("videosize between", value1, value2, "videosize");
            return (Criteria) this;
        }

        public Criteria andVideosizeNotBetween(String value1, String value2) {
            addCriterion("videosize not between", value1, value2, "videosize");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkIsNull() {
            addCriterion("txtclicklink is null");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkIsNotNull() {
            addCriterion("txtclicklink is not null");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkEqualTo(String value) {
            addCriterion("txtclicklink =", value, "txtclicklink");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkNotEqualTo(String value) {
            addCriterion("txtclicklink <>", value, "txtclicklink");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkGreaterThan(String value) {
            addCriterion("txtclicklink >", value, "txtclicklink");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkGreaterThanOrEqualTo(String value) {
            addCriterion("txtclicklink >=", value, "txtclicklink");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkLessThan(String value) {
            addCriterion("txtclicklink <", value, "txtclicklink");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkLessThanOrEqualTo(String value) {
            addCriterion("txtclicklink <=", value, "txtclicklink");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkLike(String value) {
            addCriterion("txtclicklink like", value, "txtclicklink");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkNotLike(String value) {
            addCriterion("txtclicklink not like", value, "txtclicklink");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkIn(List<String> values) {
            addCriterion("txtclicklink in", values, "txtclicklink");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkNotIn(List<String> values) {
            addCriterion("txtclicklink not in", values, "txtclicklink");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkBetween(String value1, String value2) {
            addCriterion("txtclicklink between", value1, value2, "txtclicklink");
            return (Criteria) this;
        }

        public Criteria andTxtclicklinkNotBetween(String value1, String value2) {
            addCriterion("txtclicklink not between", value1, value2, "txtclicklink");
            return (Criteria) this;
        }

        public Criteria andScoreIsNull() {
            addCriterion("score is null");
            return (Criteria) this;
        }

        public Criteria andScoreIsNotNull() {
            addCriterion("score is not null");
            return (Criteria) this;
        }

        public Criteria andScoreEqualTo(String value) {
            addCriterion("score =", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotEqualTo(String value) {
            addCriterion("score <>", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThan(String value) {
            addCriterion("score >", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThanOrEqualTo(String value) {
            addCriterion("score >=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThan(String value) {
            addCriterion("score <", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThanOrEqualTo(String value) {
            addCriterion("score <=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLike(String value) {
            addCriterion("score like", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotLike(String value) {
            addCriterion("score not like", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreIn(List<String> values) {
            addCriterion("score in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotIn(List<String> values) {
            addCriterion("score not in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreBetween(String value1, String value2) {
            addCriterion("score between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotBetween(String value1, String value2) {
            addCriterion("score not between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(Double value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(Double value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(Double value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(Double value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(Double value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(Double value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<Double> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<Double> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(Double value1, Double value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(Double value1, Double value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andDiscountpriceIsNull() {
            addCriterion("discountprice is null");
            return (Criteria) this;
        }

        public Criteria andDiscountpriceIsNotNull() {
            addCriterion("discountprice is not null");
            return (Criteria) this;
        }

        public Criteria andDiscountpriceEqualTo(Double value) {
            addCriterion("discountprice =", value, "discountprice");
            return (Criteria) this;
        }

        public Criteria andDiscountpriceNotEqualTo(Double value) {
            addCriterion("discountprice <>", value, "discountprice");
            return (Criteria) this;
        }

        public Criteria andDiscountpriceGreaterThan(Double value) {
            addCriterion("discountprice >", value, "discountprice");
            return (Criteria) this;
        }

        public Criteria andDiscountpriceGreaterThanOrEqualTo(Double value) {
            addCriterion("discountprice >=", value, "discountprice");
            return (Criteria) this;
        }

        public Criteria andDiscountpriceLessThan(Double value) {
            addCriterion("discountprice <", value, "discountprice");
            return (Criteria) this;
        }

        public Criteria andDiscountpriceLessThanOrEqualTo(Double value) {
            addCriterion("discountprice <=", value, "discountprice");
            return (Criteria) this;
        }

        public Criteria andDiscountpriceIn(List<Double> values) {
            addCriterion("discountprice in", values, "discountprice");
            return (Criteria) this;
        }

        public Criteria andDiscountpriceNotIn(List<Double> values) {
            addCriterion("discountprice not in", values, "discountprice");
            return (Criteria) this;
        }

        public Criteria andDiscountpriceBetween(Double value1, Double value2) {
            addCriterion("discountprice between", value1, value2, "discountprice");
            return (Criteria) this;
        }

        public Criteria andDiscountpriceNotBetween(Double value1, Double value2) {
            addCriterion("discountprice not between", value1, value2, "discountprice");
            return (Criteria) this;
        }

        public Criteria andSalesIsNull() {
            addCriterion("sales is null");
            return (Criteria) this;
        }

        public Criteria andSalesIsNotNull() {
            addCriterion("sales is not null");
            return (Criteria) this;
        }

        public Criteria andSalesEqualTo(Integer value) {
            addCriterion("sales =", value, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesNotEqualTo(Integer value) {
            addCriterion("sales <>", value, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesGreaterThan(Integer value) {
            addCriterion("sales >", value, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesGreaterThanOrEqualTo(Integer value) {
            addCriterion("sales >=", value, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesLessThan(Integer value) {
            addCriterion("sales <", value, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesLessThanOrEqualTo(Integer value) {
            addCriterion("sales <=", value, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesIn(List<Integer> values) {
            addCriterion("sales in", values, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesNotIn(List<Integer> values) {
            addCriterion("sales not in", values, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesBetween(Integer value1, Integer value2) {
            addCriterion("sales between", value1, value2, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesNotBetween(Integer value1, Integer value2) {
            addCriterion("sales not between", value1, value2, "sales");
            return (Criteria) this;
        }

        public Criteria andPromotionidIsNull() {
            addCriterion("promotionid is null");
            return (Criteria) this;
        }

        public Criteria andPromotionidIsNotNull() {
            addCriterion("promotionid is not null");
            return (Criteria) this;
        }

        public Criteria andPromotionidEqualTo(String value) {
            addCriterion("promotionid =", value, "promotionid");
            return (Criteria) this;
        }

        public Criteria andPromotionidNotEqualTo(String value) {
            addCriterion("promotionid <>", value, "promotionid");
            return (Criteria) this;
        }

        public Criteria andPromotionidGreaterThan(String value) {
            addCriterion("promotionid >", value, "promotionid");
            return (Criteria) this;
        }

        public Criteria andPromotionidGreaterThanOrEqualTo(String value) {
            addCriterion("promotionid >=", value, "promotionid");
            return (Criteria) this;
        }

        public Criteria andPromotionidLessThan(String value) {
            addCriterion("promotionid <", value, "promotionid");
            return (Criteria) this;
        }

        public Criteria andPromotionidLessThanOrEqualTo(String value) {
            addCriterion("promotionid <=", value, "promotionid");
            return (Criteria) this;
        }

        public Criteria andPromotionidLike(String value) {
            addCriterion("promotionid like", value, "promotionid");
            return (Criteria) this;
        }

        public Criteria andPromotionidNotLike(String value) {
            addCriterion("promotionid not like", value, "promotionid");
            return (Criteria) this;
        }

        public Criteria andPromotionidIn(List<String> values) {
            addCriterion("promotionid in", values, "promotionid");
            return (Criteria) this;
        }

        public Criteria andPromotionidNotIn(List<String> values) {
            addCriterion("promotionid not in", values, "promotionid");
            return (Criteria) this;
        }

        public Criteria andPromotionidBetween(String value1, String value2) {
            addCriterion("promotionid between", value1, value2, "promotionid");
            return (Criteria) this;
        }

        public Criteria andPromotionidNotBetween(String value1, String value2) {
            addCriterion("promotionid not between", value1, value2, "promotionid");
            return (Criteria) this;
        }

        public Criteria andPlaceidIsNull() {
            addCriterion("placeid is null");
            return (Criteria) this;
        }

        public Criteria andPlaceidIsNotNull() {
            addCriterion("placeid is not null");
            return (Criteria) this;
        }

        public Criteria andPlaceidEqualTo(String value) {
            addCriterion("placeid =", value, "placeid");
            return (Criteria) this;
        }

        public Criteria andPlaceidNotEqualTo(String value) {
            addCriterion("placeid <>", value, "placeid");
            return (Criteria) this;
        }

        public Criteria andPlaceidGreaterThan(String value) {
            addCriterion("placeid >", value, "placeid");
            return (Criteria) this;
        }

        public Criteria andPlaceidGreaterThanOrEqualTo(String value) {
            addCriterion("placeid >=", value, "placeid");
            return (Criteria) this;
        }

        public Criteria andPlaceidLessThan(String value) {
            addCriterion("placeid <", value, "placeid");
            return (Criteria) this;
        }

        public Criteria andPlaceidLessThanOrEqualTo(String value) {
            addCriterion("placeid <=", value, "placeid");
            return (Criteria) this;
        }

        public Criteria andPlaceidLike(String value) {
            addCriterion("placeid like", value, "placeid");
            return (Criteria) this;
        }

        public Criteria andPlaceidNotLike(String value) {
            addCriterion("placeid not like", value, "placeid");
            return (Criteria) this;
        }

        public Criteria andPlaceidIn(List<String> values) {
            addCriterion("placeid in", values, "placeid");
            return (Criteria) this;
        }

        public Criteria andPlaceidNotIn(List<String> values) {
            addCriterion("placeid not in", values, "placeid");
            return (Criteria) this;
        }

        public Criteria andPlaceidBetween(String value1, String value2) {
            addCriterion("placeid between", value1, value2, "placeid");
            return (Criteria) this;
        }

        public Criteria andPlaceidNotBetween(String value1, String value2) {
            addCriterion("placeid not between", value1, value2, "placeid");
            return (Criteria) this;
        }

        public Criteria andActionbtnIsNull() {
            addCriterion("actionbtn is null");
            return (Criteria) this;
        }

        public Criteria andActionbtnIsNotNull() {
            addCriterion("actionbtn is not null");
            return (Criteria) this;
        }

        public Criteria andActionbtnEqualTo(String value) {
            addCriterion("actionbtn =", value, "actionbtn");
            return (Criteria) this;
        }

        public Criteria andActionbtnNotEqualTo(String value) {
            addCriterion("actionbtn <>", value, "actionbtn");
            return (Criteria) this;
        }

        public Criteria andActionbtnGreaterThan(String value) {
            addCriterion("actionbtn >", value, "actionbtn");
            return (Criteria) this;
        }

        public Criteria andActionbtnGreaterThanOrEqualTo(String value) {
            addCriterion("actionbtn >=", value, "actionbtn");
            return (Criteria) this;
        }

        public Criteria andActionbtnLessThan(String value) {
            addCriterion("actionbtn <", value, "actionbtn");
            return (Criteria) this;
        }

        public Criteria andActionbtnLessThanOrEqualTo(String value) {
            addCriterion("actionbtn <=", value, "actionbtn");
            return (Criteria) this;
        }

        public Criteria andActionbtnLike(String value) {
            addCriterion("actionbtn like", value, "actionbtn");
            return (Criteria) this;
        }

        public Criteria andActionbtnNotLike(String value) {
            addCriterion("actionbtn not like", value, "actionbtn");
            return (Criteria) this;
        }

        public Criteria andActionbtnIn(List<String> values) {
            addCriterion("actionbtn in", values, "actionbtn");
            return (Criteria) this;
        }

        public Criteria andActionbtnNotIn(List<String> values) {
            addCriterion("actionbtn not in", values, "actionbtn");
            return (Criteria) this;
        }

        public Criteria andActionbtnBetween(String value1, String value2) {
            addCriterion("actionbtn between", value1, value2, "actionbtn");
            return (Criteria) this;
        }

        public Criteria andActionbtnNotBetween(String value1, String value2) {
            addCriterion("actionbtn not between", value1, value2, "actionbtn");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}