package com.pxene.dsp.archer.model;

import java.util.Date;

public class AdCreativeModel {
    private String id;

    private String name;

    private String adxid;

    private String adtype;

    private String txttitle;

    private String txtfirst;

    private String cbundle;

    private String apkname;

    private String txtsecond;

    private String txtshowlink;

    private Integer timelength;

    private String showlink;

    private String clicklink;

    private String materialid;

    private String categorytype;

    private String remark;

    private String flag;

    private Date createtime;

    private Date updatetime;

    private String appdesc;

    private Float apppackagesize;

    private String iconsourceid;

    private String videosize;

    private String txtclicklink;

    private String score;

    private Double price;

    private Double discountprice;

    private Integer sales;

    private String promotionid;

    private String placeid;

    private String actionbtn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getAdxid() {
        return adxid;
    }

    public void setAdxid(String adxid) {
        this.adxid = adxid == null ? null : adxid.trim();
    }

    public String getAdtype() {
        return adtype;
    }

    public void setAdtype(String adtype) {
        this.adtype = adtype == null ? null : adtype.trim();
    }

    public String getTxttitle() {
        return txttitle;
    }

    public void setTxttitle(String txttitle) {
        this.txttitle = txttitle == null ? null : txttitle.trim();
    }

    public String getTxtfirst() {
        return txtfirst;
    }

    public void setTxtfirst(String txtfirst) {
        this.txtfirst = txtfirst == null ? null : txtfirst.trim();
    }

    public String getCbundle() {
        return cbundle;
    }

    public void setCbundle(String cbundle) {
        this.cbundle = cbundle == null ? null : cbundle.trim();
    }

    public String getApkname() {
        return apkname;
    }

    public void setApkname(String apkname) {
        this.apkname = apkname == null ? null : apkname.trim();
    }

    public String getTxtsecond() {
        return txtsecond;
    }

    public void setTxtsecond(String txtsecond) {
        this.txtsecond = txtsecond == null ? null : txtsecond.trim();
    }

    public String getTxtshowlink() {
        return txtshowlink;
    }

    public void setTxtshowlink(String txtshowlink) {
        this.txtshowlink = txtshowlink == null ? null : txtshowlink.trim();
    }

    public Integer getTimelength() {
        return timelength;
    }

    public void setTimelength(Integer timelength) {
        this.timelength = timelength;
    }

    public String getShowlink() {
        return showlink;
    }

    public void setShowlink(String showlink) {
        this.showlink = showlink == null ? null : showlink.trim();
    }

    public String getClicklink() {
        return clicklink;
    }

    public void setClicklink(String clicklink) {
        this.clicklink = clicklink == null ? null : clicklink.trim();
    }

    public String getMaterialid() {
        return materialid;
    }

    public void setMaterialid(String materialid) {
        this.materialid = materialid == null ? null : materialid.trim();
    }

    public String getCategorytype() {
        return categorytype;
    }

    public void setCategorytype(String categorytype) {
        this.categorytype = categorytype == null ? null : categorytype.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public String getAppdesc() {
        return appdesc;
    }

    public void setAppdesc(String appdesc) {
        this.appdesc = appdesc == null ? null : appdesc.trim();
    }

    public Float getApppackagesize() {
        return apppackagesize;
    }

    public void setApppackagesize(Float apppackagesize) {
        this.apppackagesize = apppackagesize;
    }

    public String getIconsourceid() {
        return iconsourceid;
    }

    public void setIconsourceid(String iconsourceid) {
        this.iconsourceid = iconsourceid == null ? null : iconsourceid.trim();
    }

    public String getVideosize() {
        return videosize;
    }

    public void setVideosize(String videosize) {
        this.videosize = videosize == null ? null : videosize.trim();
    }

    public String getTxtclicklink() {
        return txtclicklink;
    }

    public void setTxtclicklink(String txtclicklink) {
        this.txtclicklink = txtclicklink == null ? null : txtclicklink.trim();
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score == null ? null : score.trim();
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscountprice() {
        return discountprice;
    }

    public void setDiscountprice(Double discountprice) {
        this.discountprice = discountprice;
    }

    public Integer getSales() {
        return sales;
    }

    public void setSales(Integer sales) {
        this.sales = sales;
    }

    public String getPromotionid() {
        return promotionid;
    }

    public void setPromotionid(String promotionid) {
        this.promotionid = promotionid == null ? null : promotionid.trim();
    }

    public String getPlaceid() {
        return placeid;
    }

    public void setPlaceid(String placeid) {
        this.placeid = placeid == null ? null : placeid.trim();
    }

    public String getActionbtn() {
        return actionbtn;
    }

    public void setActionbtn(String actionbtn) {
        this.actionbtn = actionbtn == null ? null : actionbtn.trim();
    }
}